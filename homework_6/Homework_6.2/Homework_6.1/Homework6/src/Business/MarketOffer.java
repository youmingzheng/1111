/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author caili
 */
public class MarketOffer {
    private Product product;
    private Market market;
    private int floor;
    private int target;
    private int ceiling;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public float getFloor() {
        return floor;
    }

    public float getTarget() {
        return target;
    }

    public float getCeiling() {
        return ceiling;
    }

    public void setRangePrice() {
        floor = product.getPrice() + market.getAdvisedFloor();
        ceiling = product.getPrice() + market.getAdvisedCeiling();
        target = product.getPrice() + market.getAdvisedTarget();
    }
    
    public String toString(){
        return product.getProductName();
    }
}
