/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class MarketList {
    private ArrayList<Market> marketList;

    public ArrayList<Market> getMarketList() {
        return marketList;
    }

    public void setMarketList(ArrayList<Market> marketList) {
        this.marketList = marketList;
    }
    
    public Market newMarket(){
        Market market = new Market();
        marketList.add(market);
        return market;
    }
    
    public MarketList(){
        marketList=new ArrayList();
    }
}