/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author caili
 */
public class Order {
    private Customer customer;
    private Salesman salesman;
    private MarketOffer marketOffer;
    private int quantity;
    private int actualPrice;
    private int total;
    private int gap;
    private Market market;
    private Product product;

    public Product getProduct() {
        return marketOffer.getProduct();
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    
    public void setMarket(Market market) {
        this.market = market;
    }

    public Market getMarket() {
        return marketOffer.getMarket();
    }


    public int getGap() {
        return (int)((actualPrice*quantity)-(marketOffer.getTarget()*quantity));
    }

    public void setGap(int gap) {
        this.gap = gap;
    }

    public int getTotal() {
        return actualPrice*quantity;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Salesman getSalesman() {
        return salesman;
    }

    public void setSalesman(Salesman salesman) {
        this.salesman = salesman;
    }

public MarketOffer getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(MarketOffer marketOffer) {
        this.marketOffer = marketOffer;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(int actualPrice) {
        this.actualPrice = actualPrice;
    }
    
    
    public int revenue(){
        int revenue = (actualPrice
                - marketOffer.getProduct().getPrice())
                * quantity;
        return revenue;
    }
    
    public int target(){
        int rev = (int) ((actualPrice
                - marketOffer.getTarget())
                * quantity);
        return rev;
    }

    public int getTarget() {
        return target();
    }
    
    public String toString(){
        return marketOffer.getProduct().getProductName();
    }
    
}