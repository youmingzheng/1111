/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 *
 * @author Youming Zheng
 */
public class UserAccountDirectory {
    private ArrayList<UserAccount> userAccountDirectory;
    
    public UserAccountDirectory(){
        userAccountDirectory=new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(ArrayList<UserAccount> userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }
    
    public UserAccount newUserAccount(){
        UserAccount ua=new UserAccount();
        userAccountDirectory.add(ua);
        return ua;
    }
    
    public void removeUserAccount(UserAccount ua){
        userAccountDirectory.remove(ua);
    }
    
    public UserAccount findByUserName(String userName){
        for(UserAccount ua:userAccountDirectory){
            if(ua.getUserName().equals(userName)){
                return ua;
            }
        }
        return null;
    }
    
    public UserAccount isValidUser(String userName, String password){
        for(UserAccount ua:userAccountDirectory){
            if(ua.getUserName().equals(userName) && ua.getPassword().equals((ua.getPassword()))){
                return ua;
            }
        }
        return null;
    }
}
