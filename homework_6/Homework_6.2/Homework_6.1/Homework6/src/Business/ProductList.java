/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class ProductList {
    private ArrayList<Product> productList;
    
    public ProductList(){
        productList = new ArrayList<Product>();
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }
    
    public Product addProduct(){
        Product product = new Product();
        productList.add(product);
        return product;
    }
    
    public void deleteProduct(Product product1){
        productList.remove(product1);
    }
    
    public Product findProduct(String num){
        for(Product product: productList){
            if(product.getModelNumber()==Integer.valueOf(num)){
                return product;
            }
        } 
        return null;
    }
   
    public Product newProduct(){
        Product product = new Product();
        productList.add(product);
        return product;
    }
}
