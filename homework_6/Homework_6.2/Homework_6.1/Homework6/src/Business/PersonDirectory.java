/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Youming Zheng
 */
public class PersonDirectory {
    private ArrayList<Person> personDirectory;
    
    public PersonDirectory(){
        personDirectory=new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }
    
    public Person newPerson(){
        Person p=new Person();
        personDirectory.add(p);
        return p;
    }
    
    public void removePerson(Person p){
        personDirectory.remove(p);
    }
    
    public Person findByName(String name){
        for(Person p:personDirectory){
            if(p.getFirstName().equals(name)){
                return p;
            }
        }
        return null;
    }
}
