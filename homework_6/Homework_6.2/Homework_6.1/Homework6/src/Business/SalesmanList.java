/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author Emmet Zhao
 */
public class SalesmanList {
    private ArrayList<Salesman> salesmanList;

    public ArrayList<Salesman> getSalesmanList() {
        return salesmanList;
    }

    public void setSalesmanList(ArrayList<Salesman> salesmanList) {
        this.salesmanList = salesmanList;
    }
    
    public SalesmanList(){
        salesmanList=new ArrayList<>();
    }
    
    public Salesman newSalesman(){
        Salesman salesman=new Salesman();
        salesmanList.add(salesman);
        return salesman;
    }
}
