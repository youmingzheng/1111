/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class Market {
    private String marketName;
    private ArrayList<Customer> customerList;
    private int advisedFloor;
    private int advisedCeiling;
    private int advisedTarget;

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        customerList = new ArrayList<>();
        this.marketName = marketName;
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }

    public int getAdvisedFloor() {
        return advisedFloor;
    }

    public void setAdvisedFloor(int advisedFloor) {
        this.advisedFloor = advisedFloor;
    }

    public int getAdvisedCeiling() {
        return advisedCeiling;
    }

    public void setAdvisedCeiling(int advisedCeiling) {
        this.advisedCeiling = advisedCeiling;
    }

    public int getAdvisedTarget() {
        return advisedTarget;
    }

    public void setAdvisedTarget(int advisedTarget) {
        this.advisedTarget = advisedTarget;
    }
    
    public int getRevenue(){
        return revenue();
    }
    
    @Override
    public String toString(){
        return marketName;
    }

    public int revenue (){
        int revenue = 0;
        for(Customer customer : customerList){
            revenue = 
                    customer.getRevenue()
                    + revenue;
        }
        return revenue;
    }
    
}
