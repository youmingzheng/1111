/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author caili
 */
public class Business {
    private String name;
    private MarketList marketList;
    private MarketOfferList marketOfferList;
    private OrderList orderList;
    private ProductList productList;
    private SalesmanList salemanList;
    private PersonDirectory personDirectory;
    private UserAccountDirectory userAccountDirectory;
    private CustomerList customerList;
    
    public Business(String name){
        this.name = name;
        this.productList = new ProductList();
        this.personDirectory = new PersonDirectory();
        this.userAccountDirectory = new UserAccountDirectory();
        this.marketList=new MarketList();
        this.orderList=new OrderList();
        this.marketOfferList=new MarketOfferList();
        this.salemanList=new SalesmanList();
        this.customerList = new CustomerList();
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    
    public PersonDirectory getPersonDirectory() {
        return personDirectory;
    }

    public void setPersonDirectory(PersonDirectory personDirectory) {
        this.personDirectory = personDirectory;
    }

    public CustomerList getCustomerList() {
        return customerList;
    }

    public void setCustomerList(CustomerList customerList) {
        this.customerList = customerList;
    }

    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public MarketOfferList getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(MarketOfferList marketOfferList) {
        this.marketOfferList = marketOfferList;
    }

    public OrderList getOrderList() {
        return orderList;
    }

    public void setOrderList(OrderList orderList) {
        this.orderList = orderList;
    }

    public SalesmanList getSalemanList() {
        return salemanList;
    }

    public void setSalemanList(SalesmanList salemanList) {
        this.salemanList = salemanList;
    }
    
    
    
}
