/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class OrderList {
    private ArrayList<Order> orderList;
    private int revenue;

    public OrderList() {
        ArrayList<Order> orderList = new ArrayList<>();
        this.orderList = orderList;
    }
    
    public ArrayList<Order> getOrderList() {
        return orderList;
    }
    
    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }

    public int getRevenue() {
        return revenue;
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }
    
    public Order newOrder(){
        Order order=new Order();
        orderList.add(order);
        return order;
    }
    
    public void removeOrder(Order order){
        orderList.remove(order);
    }
    
}
