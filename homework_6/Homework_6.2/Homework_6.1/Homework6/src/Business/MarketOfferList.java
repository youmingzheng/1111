/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class MarketOfferList {
    private ArrayList<MarketOffer> marketOfferList;

    public MarketOfferList() {
        ArrayList<MarketOffer> marketOfferList = new ArrayList<>();
        this.marketOfferList = marketOfferList;
    }

    public ArrayList<MarketOffer> getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(ArrayList<MarketOffer> marketOfferList) {
        this.marketOfferList = marketOfferList;
    }
    
    public void AddOffer(Market market){
        MarketOffer offer = new MarketOffer();
        this.marketOfferList.add(offer);
        offer.setMarket(market);
    }
    
    public MarketOffer newMarketOffer(){
        MarketOffer marketOffer=new MarketOffer();
        marketOfferList.add(marketOffer);
        return marketOffer;
    }
}
