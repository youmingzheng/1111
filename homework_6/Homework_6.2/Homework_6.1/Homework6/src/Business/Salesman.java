/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class Salesman {
    private String salesmanName;
    private ArrayList<Customer> customerList;
    private int revenue;

    public Salesman() {
        ArrayList<Customer> customerList = new ArrayList<>();
        this.customerList = customerList;
    }
    

    public String getSalesmanName() {
        return salesmanName;
    }

    public void setSalesmanName(String salesmanName) {
        this.salesmanName = salesmanName;
    }

    public int getRevenue() {
        return revenue();
    }

    public void setRevenue(int revenue) {
        this.revenue = revenue;
    }

    public ArrayList<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(ArrayList<Customer> customerList) {
        this.customerList = customerList;
    }
    
    public int revenue(){
        int rev = 0;
        for(Customer customer : customerList){
            rev = customer.getRevenue() + rev;
        }
        return rev;
    }

    public int getTarget() {
        int tgt = 0;
        for(Customer customer : customerList){
            tgt = tgt + customer.getTarget();
        }
        return tgt;
    }
    
    @Override
    public String toString(){
        return salesmanName;
    }
    
}
