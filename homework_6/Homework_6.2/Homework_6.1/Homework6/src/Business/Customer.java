/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author caili
 */
public class Customer {
    private String customerName;
    private String type;
    private ArrayList<Order> orderList;
    private Market market;

    public Customer() {
        ArrayList<Order> orderList = new ArrayList<>();
        this.orderList = orderList;
    }

    
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public int getRevenue() {
        return revenue();
    }
    
    
    public int revenue (){
        int revenue = 0;
        for(Order order : orderList){
            revenue = revenue + order.revenue();
        }
        return revenue;
    }

    public int getTarget() {
        int tgt = 0;
        for(Order order : orderList){
            tgt = tgt + order.getTarget();
        }
        return tgt;
    }
    public String toString(){
        return customerName;
    }
}
