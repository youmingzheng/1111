/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ConfigureBusiness;

import Business.*;
import java.security.NoSuchAlgorithmException;
/**
 *
 * @author Emmet Zhao
 */

public class ConfigureABusiness {
    public static Business Initialize(String name){
        Business b = new Business(name);
        
        PersonDirectory pd=b.getPersonDirectory();
        Person p;
        p=pd.newPerson();
        p.setFirstName("John");
        p.setLastName("Adam");
        
        p=pd.newPerson();
        p.setFirstName("Tom");
        p.setLastName("Jerry");
        
        Salesman salesman1=b.getSalemanList().newSalesman();
        salesman1.setSalesmanName("salesman1");
        
        Salesman salesman2=b.getSalemanList().newSalesman();
        salesman2.setSalesmanName("salesman2");
        
        Salesman salesman3=b.getSalemanList().newSalesman();
        salesman3.setSalesmanName("salesman3");
        
        UserAccountDirectory uad=b.getUserAccountDirectory();
        Person p2=pd.findByName("John");
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("hehehe");
            ua.setPassword("111111");
            ua.setRole("Product Manager");
            ua.setStatus(true);
        }
        
        p2=pd.findByName("Tom");
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("lalala");
            ua.setPassword("111111");
            ua.setRole("Executive Manager");
            ua.setStatus(true);
        }
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("salesman1");
            ua.setPassword("111111");
            ua.setRole("Salesman");
            ua.setStatus(true);
            ua.setSalesman(salesman1);
        }
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("salesman3");
            ua.setPassword("111111");
            ua.setRole("Salesman");
            ua.setStatus(true);
            ua.setSalesman(salesman3);
        }
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("salesman2");
            ua.setPassword("111111");
            ua.setRole("Salesman");
            ua.setStatus(true);
            ua.setSalesman(salesman2);
        }
        
        if(p2!=null){
            UserAccount ua=uad.newUserAccount();
            ua.setPerson(p2);
            ua.setUserName("market");
            ua.setPassword("111111");
            ua.setRole("Market Manager");
            ua.setStatus(true);
        }
        
        
        Product product1=b.getProductList().addProduct();
        product1.setProductName("product1.0");
        product1.setPrice(200);
        
        Product product2=b.getProductList().addProduct();
        product2.setProductName("product2.0");
        product2.setPrice(300);
        
        Product product21=b.getProductList().addProduct();
        product21.setProductName("product2.1");
        product21.setPrice(310);
        
        Market market1=b.getMarketList().newMarket();
        market1.setMarketName("market1");
        market1.setAdvisedCeiling(500);
        market1.setAdvisedFloor(100);
        market1.setAdvisedTarget(200);
        
        Market market2=b.getMarketList().newMarket();
        market2.setMarketName("market2");
        market2.setAdvisedCeiling(100);
        market2.setAdvisedFloor(20);
        market2.setAdvisedTarget(50);
        
        MarketOffer marketOffer0=b.getMarketOfferList().newMarketOffer();
        marketOffer0.setProduct(product1);
        marketOffer0.setMarket(market1);
        marketOffer0.setRangePrice();
        
        MarketOffer marketOffer1=b.getMarketOfferList().newMarketOffer();
        marketOffer1.setProduct(product2);
        marketOffer1.setMarket(market2);
        marketOffer1.setRangePrice();
        
        MarketOffer marketOffer2=b.getMarketOfferList().newMarketOffer();
        marketOffer2.setProduct(product21);
        marketOffer2.setMarket(market2);
        marketOffer2.setRangePrice();
        
        MarketOffer marketOffer3=b.getMarketOfferList().newMarketOffer();
        marketOffer3.setProduct(product1);
        marketOffer3.setMarket(market2);
        marketOffer3.setRangePrice();
        
        
        Order order1=b.getOrderList().newOrder();
        order1.setActualPrice(1000);
        order1.setQuantity(2);
        order1.setMarketOffer(marketOffer0);
        order1.setSalesman(salesman3);
        
        Order order2=b.getOrderList().newOrder();
        order2.setActualPrice(150);
        order2.setQuantity(2);
        order2.setMarketOffer(marketOffer1);
        order2.setSalesman(salesman1);
        
        Order order3=b.getOrderList().newOrder();
        order3.setActualPrice(1000);
        order3.setQuantity(3);
        order3.setMarketOffer(marketOffer0);
        order3.setSalesman(salesman2);
        
        Order order4=b.getOrderList().newOrder();
        order4.setActualPrice(700);
        order4.setQuantity(9);
        order4.setMarketOffer(marketOffer1);
        order4.setSalesman(salesman2);
        
        Order order5=b.getOrderList().newOrder();
        order5.setActualPrice(4);
        order5.setQuantity(8);
        order5.setMarketOffer(marketOffer1);
        order5.setSalesman(salesman3);
        
        
        return b;
    }
}
